# Script Collection for Intro NLP lecture project

This repo contains a set of scripts I use for the lecture project to Introduction to Natural Language Processing.

The script assume the following folder structure:

```
foo/docs/dXXX/...
foo/eval/t1.rouge.in and other files
foo/scripts/THIS-REPO
```

## combine.py

This script reads from input file for rouge (e.g. `t1.rouge.in`) the models and creates a YAML file containing the models and the original document.

An example output looks like this (content is stored under `foo/scripts/output/combine_D30002_APW19981031_0720.txt` when running the script):

```
# Looking at ID=66 D30002/APW19981031.0720

models:
- Former-Hurricane Mitch leaves 231 dead in Honduras, 357 in region
- At least 231 killed by Hurricane Mitch in Honduras; makes region toll 357.
- Honduras confirms 231 people dead in the wake of Hurricane Mitch
- Hurricane Mitch's death toll is 357, including at least 231 in Honduras

document: At least 231 people have been confirmed dead in Honduras from former-hurricane
  Mitch, bringing the storm's death toll in the region to 357, the National Emergency
  Commission said Saturday. Mitch _ once, 2nd graf pvs
```

To generate this file, run `python combine.py 66`. If you want to process all files defined in the `t1.rouge.in`, then use

```
python combine.py all
```

To run the script, you might need to install `ElementTree` and `pyyaml`.

