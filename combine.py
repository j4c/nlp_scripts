#!/usr/bin/python

# Author: Julian Viereck <jviereck@student.ethz.ch>

# This script takes a rouge input file and builds one file per document, that
# contains the original input document text and the models for it.

import re
import os.path
from collections import OrderedDict

import yaml
import xml.etree.ElementTree as ET

import argparse

# !!! YOU MIGHT WANT TO ADJUST THESE PATHS !!!
rouge_filename = 't1.rouge.in'
eval_path = os.path.join('..', 'eval')
docs_path = os.path.join('..', 'docs')

# Using an OrderedDict to keep the order of the dumped keys in sync with the way
# the dict is set in the program (e.g. have the models go first, then the document, ...)
# For this to work, it's required to tell PyYaml how to treat OrderedDict.
#
# Source: http://stackoverflow.com/questions/9951852/pyyaml-dumping-things-backwards
def order_rep(dumper, data):
    return dumper.represent_mapping( u'tag:yaml.org,2002:map', data.items(), flow_style=False )
yaml.add_representer( OrderedDict, order_rep )

def read_file(path):
  f = open(path)
  content = f.read()
  f.close()
  return content.strip()

def write_file(path, content):
  print 'Writing file: ' + path
  f = open(path, 'w')
  f.write(content)
  f.close()

def combine_id(eval_id):
  print 'combining rouge input file id=' + eval_id
  # This python object will contain the combined information that eventuall
  # get's dumped in the the YAML file.
  res = OrderedDict()

  eval_node = root.findall("./EVAL/[@ID='" + eval_id + "']")[0]

  models = eval_node.findall('./MODELS')[0]

  # Extract the directory/name to original document we work on here
  raw_name = models[0].text
  [doc_dir, doc_filename] = re.search('\/(D\d+)\.P\.10\.T\.\w\.(.+)$', raw_name).groups()

  # Load the models.
  res['models'] = [read_file(os.path.join(eval_path, 'models', model.text)) for model in models]

  # Get the original document
  doc_path = os.path.join(docs_path, doc_dir + 't',  doc_filename)

  content = read_file(doc_path).replace('&AMP;', 'AMP')
  res['document'] = ET.fromstring(content).find('TEXT').text.replace('\n', ' ').replace('  ', ' ').strip()

  # Start writing the output
  out =  '# Looking at rouge eval-id=%s %s/%s\n' % (eval_id, doc_dir, doc_filename);

  out += yaml.dump(res, default_flow_style=False)

  # Add an extra newline in front of the top level entry names
  out = re.sub("^(\w+):", "\n\g<1>:", out, flags=re.M)

  s = lambda str: str.replace('.', '_').replace('/', '_')

  write_file(os.path.join('output', 'combine_' + s(doc_dir) + '_' + s(doc_filename)) + '.txt', out)

## =============================================================================

parser = argparse.ArgumentParser(description='Combines relevant information for one evaluation ID')
parser.add_argument('eval_id', metavar='ID', default='1', type=str, nargs='+',
                   help='The evalution id from the rouge input script to summarize')

args = parser.parse_args()

# Read the rouge configuration file
root = ET.parse(os.path.join(eval_path, rouge_filename)).getroot()

# We assume there is a folder "output" that the files will be stored into.
try:
    os.stat('output')
except:
    os.mkdir('output')

# The eval id in the rouge script we want to summarize on.
if 'all' in args.eval_id:
  size = len(root.getchildren())
  i = 1
  while i < size:
    combine_id(str(i))
    i = i + 5
else:
  for eval_id in args.eval_id:
    combine_id(eval_id)

